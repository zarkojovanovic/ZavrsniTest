use zavrsnitest

INSERT INTO destination (name) values
	('Bec'),
	('Pariz'),
	('Prag');
INSERT INTO category (name) values
	('Letovanje'),
	('Zimovanje'),
	('Rafting');

INSERT INTO travel (destination_id, date_go, date_return, category_id, price, plan) values
	(1, '21-5-2102', '122-5-250', 2, 556.21, 'bla'),
	(2, '21-6-2102', '122-6-250', 2, 656.21, 'bla bla'),
	(1, '21-7-2102', '122-5-250', 2, 756.21, 'bla bla bla'),
	(3, '21-8-2102', '122-8-250', 2, 856.21, 'blablalala');
	
-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name, role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin', 'ADMINISTRATOR');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name, role) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic', 'WORKER');
