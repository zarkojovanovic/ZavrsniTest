package vp.advancedjava.students.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.repository.CategoryRepository;

@Component
public class CategoryService {
	@Autowired
	CategoryRepository categoryRepo;
	
	public List<Category> findAll() {
		return categoryRepo.findAll();
	}
	
	public Category findOne(Long id) {
		return categoryRepo.findOne(id);
	}
	
	public Category save(Category category) {
		return categoryRepo.save(category);
	}
	
	public void remove(Long id) {
		categoryRepo.delete(id);
	}
}
