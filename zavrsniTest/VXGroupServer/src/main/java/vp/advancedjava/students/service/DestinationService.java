package vp.advancedjava.students.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Destination;
import vp.advancedjava.students.repository.DestinationRepository;

@Component
public class DestinationService {
	
	@Autowired
	DestinationRepository destinationRepo;
	
	public List<Destination> findAll() {
		return destinationRepo.findAll();
	}
	
	public Destination findOne(Long id) {
		return destinationRepo.findOne(id);
	}
	
	public Destination save(Destination destination) {
		return destinationRepo.save(destination);
	}
	
	public void remove(Long id) {
		destinationRepo.delete(id);
	}
}
