package vp.advancedjava.students.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.Travel;

@Component
public interface TravelRepository extends JpaRepository<Travel, Long> {
	
	Page<Travel> findByCategoryAndPriceBetween( Category category, double lowestPrice, double highestPrice, Pageable page );
	Page<Travel> findByPriceBetween( double lowestPrice, double highestPrice, Pageable page);
	Page<Travel> findByCategory(Category category, Pageable page); 
}
