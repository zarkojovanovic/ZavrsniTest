package vp.advancedjava.students.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.Travel;
import vp.advancedjava.students.service.CategoryService;
import vp.advancedjava.students.service.TravelService;

@RestController
public class TravelController {
	
	@Autowired
	TravelService travelService;
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping(value= "api/travel")
	public Page<Travel> getAll(Pageable page,@RequestParam(required=false, defaultValue="") String categoryId,
			@RequestParam(required=false, defaultValue="") String lowestPrice,
			@RequestParam(required=false, defaultValue="") String highestPrice) {
		final Page<Travel> retVal;
		Double low = 0.0;
		Double high = 0.0;
		if (!lowestPrice.isEmpty() && !highestPrice.isEmpty()) { 
		  low = Double.valueOf(lowestPrice);
		  high = Double.valueOf(highestPrice);
		}
		if (!categoryId.isEmpty() && !lowestPrice.isEmpty() && !highestPrice.isEmpty() ) {
			 Category category = categoryService.findOne(Long.valueOf(categoryId));
			 retVal = travelService.findByCategoryAndByPrice(page, category, low, high);
		} else if (!categoryId.isEmpty() && lowestPrice.isEmpty() && highestPrice.isEmpty()) {
			 Category category = categoryService.findOne(Long.valueOf(categoryId));
			 retVal = travelService.findByCategory(page, category);
		} else if (categoryId.isEmpty() && !lowestPrice.isEmpty() && !highestPrice.isEmpty()) {
			retVal = travelService.findByPrice(page, low, high);
		} else {
			retVal = travelService.findAll(page);
		}
		
		return retVal;
	}
	
	@GetMapping(value ="api/travel/{id}")
	public Travel getOne(@PathVariable Long id) {
		final Travel retVal = travelService.findOne(id);
		return retVal;
	}
	@SuppressWarnings("rawtypes")
	@DeleteMapping(value= "api/travel/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		if (travelService.findOne(id) != null) {
			travelService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else 
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "api/travel", method = RequestMethod.POST)
    public Travel create(@RequestBody Travel travel) {
        final Travel retVal = travelService.save(travel);
        return retVal;
    }

	
}
