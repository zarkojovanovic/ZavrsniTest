package vp.advancedjava.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.Travel;
import vp.advancedjava.students.service.CategoryService;

@RestController
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping(value = "api/categories")
	public List<Category> getAll() {
		final List<Category> retVal = categoryService.findAll();
		
		return retVal;
	}
	
}
