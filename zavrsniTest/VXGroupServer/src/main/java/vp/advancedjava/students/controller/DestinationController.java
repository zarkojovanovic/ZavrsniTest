package vp.advancedjava.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.Destination;
import vp.advancedjava.students.model.Travel;
import vp.advancedjava.students.service.DestinationService;

@RestController
public class DestinationController {
	
	@Autowired
	DestinationService destinationService;
	
	@GetMapping(value = "api/destinations")
	public List<Destination> getAll() {
		final List<Destination> retVal = destinationService.findAll();
		
		return retVal;
	}
	@RequestMapping(value = "api/destinations", method = RequestMethod.POST)
    public Destination create(@RequestBody Destination dest) {
        final Destination retVal = destinationService.save(dest);
        return retVal;
    }
}
