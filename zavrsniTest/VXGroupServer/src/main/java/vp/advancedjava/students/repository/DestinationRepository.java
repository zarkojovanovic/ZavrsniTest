package vp.advancedjava.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Destination;

@Component
public interface DestinationRepository extends JpaRepository<Destination, Long> {

}
