package vp.advancedjava.students.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Travel {
	
	@Id
	@GeneratedValue
	private Long id;
		
	@ManyToOne(fetch = FetchType.EAGER)
	private Destination destination;
	
	private String dateGo;
	private String dateReturn;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;
	
	private double price;
	
	private String plan;

	private Travel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public String getDateGo() {
		return dateGo;
	}

	public void setDateGo(String dateGo) {
		this.dateGo = dateGo;
	}

	public String getDateReturn() {
		return dateReturn;
	}

	public void setDateReturn(String dateReturn) {
		this.dateReturn = dateReturn;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}
	
	
	
}
