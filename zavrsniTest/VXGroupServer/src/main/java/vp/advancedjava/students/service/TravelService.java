package vp.advancedjava.students.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.Travel;
import vp.advancedjava.students.repository.TravelRepository;

@Component
public class TravelService {
	
	@Autowired
	TravelRepository travelRepo;
	
	public Page<Travel> findAll(Pageable page) {
		return travelRepo.findAll(page);
	}
	
	public Travel findOne(Long id) {
		return travelRepo.findOne(id);
	}
	
	public Travel save(Travel travel) {
		return travelRepo.save(travel);
	}
	
	public void remove(Long id) {
		travelRepo.delete(id);
	}
	
	public Page<Travel> findByCategoryAndByPrice(Pageable page, Category category, double lowestPrice,
			double highestPrice) {
		return travelRepo.findByCategoryAndPriceBetween( category, lowestPrice, highestPrice, page);
	}
	public Page<Travel> findByPrice(Pageable page, double lowestPrice, double highestPrice) {
		return travelRepo.findByPriceBetween(lowestPrice, highestPrice, page);
	}
	public Page<Travel> findByCategory(Pageable page, Category category) {
		return travelRepo.findByCategory(category, page);
	}
}
