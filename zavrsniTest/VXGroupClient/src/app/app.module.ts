import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';

import { AuthenticationService } from './service/authentication-service.service';
import { JwtUtilsService } from './service/jwt-utils.service';
import { TokenInterceptorService } from './service/token-interceptor.service';
import { CanActivateAuthGuard } from './service/can-activate-auth.guard';
import { MainComponent } from './page/main/main.component';
import { CategoryService } from './service/category.service';
import { DestinationService } from './service/destination.service';
import { TravelService } from './service/travel.service';
import { AddComponent } from './add/add.component';

const appRoutes: Routes = [
  
  { path: 'main', component: MainComponent},
  { path: 'login', component: LoginComponent},
  { path: 'add', component: AddComponent},
  { path: 'add/:id', component: AddComponent},
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {path: '**', component: PageNotFoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginComponent,
    MainComponent,
    AddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService,
    CategoryService,
    DestinationService,
    TravelService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
