import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TravelService } from '../../service/travel.service';
import { CategoryService } from '../../service/category.service';
import { DestinationService } from '../../service/destination.service';
import { Travel } from '../../model/travel-model';
import { Category } from '../../model/category-model';
import { Destination } from '../../model/destination-model';
import { AuthenticationService } from '../../service/authentication-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {
  categoryToAdd: Category = {
    name: '',
  }
  plan: string;
  travels: Travel[];
  categories: Category[];
  destinations: Destination[];
  currentPage: number;
  totalPages: number;
  filterData={categoryId:'', lowestPrice:'', highestPrice:''};
  constructor(private travelService: TravelService,
              private categoryService: CategoryService,
              private destinationService: DestinationService,
              private authService: AuthenticationService,
            private router: Router) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.travelService.getTravels(this.currentPage).subscribe(
      (resp: Travel[]) => {
        this.travels = resp['content'];
        this.totalPages = resp['totalPages'];
      });
    this.categoryService.getCategories().subscribe(
      (resp: Category[]) => {
        this.categories = resp;
   
      });
    this.destinationService.getDestinations().subscribe(
        (resp: Destination[]) => {
          this.destinations = resp;
         
        });      
  }
  
  filter() {
    this.travelService.getTravels(this.currentPage, this.filterData.categoryId,
       this.filterData.lowestPrice, this.filterData.highestPrice).subscribe(
         (resp: Travel[]) => {
           this.travels = resp['content'];
           this.totalPages = resp['totalPages'];
         }); 

       
  }
  changePage(i:number){
    this.currentPage+=i;
    this.loadData();

  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  getTableClass(){
    if(this.isLoggedIn()===true){
      return "col-sm-12";
    }
    else{
      return "col-sm-8";
    }
  }
  delete(itemId: number) {
    this.travelService.delete(itemId).subscribe(
      (resp: string) =>{
        this.loadData();
      });
  }
  addTravel() {
    this.router.navigate(['/add']);
    this.loadData();
  }
  addCategory() {
    this.categoryService.save(this.categoryToAdd).subscribe(
      (resp: Category) => {
        this.loadData();
        this.reset()
      });
  }
  addDest() {
    this.destinationService.save(this.categoryToAdd).subscribe(
      (resp: Destination) => {
        this.loadData();
        this.reset();
      });
  }
  reset() {
    this.categoryToAdd = {
      name: ''
    }
  }
  showPlan(plan) {
    this.plan = plan;
  }
 
  

}
