import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';  
import { Category } from '../model/category-model';
@Injectable()
export class CategoryService {
  private readonly path: string = 'api/categories';
  constructor(private httpClient: HttpClient) { }

  getCategories() {
    return this.httpClient.get(this.path)
      .catch((error: any) => 
        Observable.throw(error.message || 'Server error')
      );
  }
  getOne(id: number) {
    return this.httpClient.get(this.path+ "/" +id);
  } 
  save(category: Category) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json'});
    return this.httpClient.post(this.path, category, {headers});
   
  } 
}
