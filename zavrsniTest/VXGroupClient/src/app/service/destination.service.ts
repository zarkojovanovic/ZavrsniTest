import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Destination } from '../model/destination-model';


@Injectable()
export class DestinationService {
  private readonly path: string = 'api/destinations';
  constructor(private httpClient: HttpClient) { }

  getDestinations() {
    return this.httpClient.get(this.path)
      .catch((error: any) => 
        Observable.throw(error.message || 'Server error')
      );
  }
  getOne(id: number) {
    return this.httpClient.get(this.path+ "/" +id);
  }  
  save(destination: Destination) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json'});
    return this.httpClient.post(this.path, destination, {headers});
   
  } 
}
