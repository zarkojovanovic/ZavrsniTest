import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Travel } from '../model/travel-model';

@Injectable()
export class TravelService {

  private readonly path: string = 'api/travel';

  constructor(private httpClient: HttpClient) { }

  getTravels(currentPage: number, categoryId?: string, lowestPrice?: string, highestPrice?: string ) {
    let page = String(currentPage);
    let params: HttpParams = new HttpParams();

    if(+categoryId) {
      params = params.append('categoryId', categoryId);
    }
    if (+lowestPrice) {
			params = params.append('lowestPrice', lowestPrice);
		}
		if (+highestPrice){
			params = params.append('highestPrice', highestPrice);
    }
    return this.httpClient.get<Travel[]>(this.path, {params})
      .catch((error: any) =>
				Observable.throw(error.message || 'Server error')
			);
  }
  getOne(id:number){
    return this.httpClient.get(this.path+"/"+id).catch((error: any) =>
    Observable.throw(error.message || 'Server error')
  );
  }

  delete(itemId: number) {
    let headers = new HttpHeaders({'Content-Type': 'application/text'});
    return this.httpClient.delete(this.path+ "/" + itemId, { responseType: 'text', headers });
  }

  save(travel: Travel) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json'});
    return this.httpClient.post(this.path, travel, {headers});
   
  }

}
