import { Category } from './category-model'; 
import { Destination } from './destination-model'; 

export class Travel implements TravelInterface {
    public destination: Destination;
    public dateGo: string;
    public dateReturn: string;
    public category: Category;
    public price: number;
    public plan: string;

    constructor(travelCfg: TravelInterface) {
        this.destination = travelCfg.destination;
        this.dateGo = travelCfg.dateGo;
        this.dateReturn = travelCfg.dateReturn;
        this.category = travelCfg.category;
        this.price = travelCfg.price;
        this.plan = travelCfg.plan;
    }
}

interface TravelInterface {
    destination: Destination;
    dateGo: string;
    dateReturn: string;
    category: Category;
    price: number;
    plan: string;
}