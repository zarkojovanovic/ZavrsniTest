export class Destination implements DestinationInterface {
  
    public name: string;

    constructor(destinationCfg: DestinationInterface) {
      
        this.name = destinationCfg.name;
    }
}

interface DestinationInterface {
   
    name: string;
}