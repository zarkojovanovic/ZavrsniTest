import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Travel } from '../model/travel-model';
import { Category } from '../model/category-model';
import { Destination } from '../model/destination-model';
import { TravelService } from '../service/travel.service';
import { DestinationService } from '../service/destination.service';
import { CategoryService } from '../service/category.service';
import { Router, Route, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddComponent implements OnInit {
   idRoute : string = this.route.snapshot.paramMap.get('id');
  categories: Category[];
  destinations: Destination[];
  travelsToAdd: Travel= {
    destination: {
      name: ''
    },
    dateGo: '',
    dateReturn: '',
    category: {
      name: ''
    },
    price: 0,
    plan: ''
  }
  constructor(private travelService: TravelService,
              private destinationService: DestinationService,
              private categoryService: CategoryService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadData();
  }

  
  loadData() {
    if (this.idRoute != null) {
    this.route.params.subscribe((params)=>{
      var id = +params['id'];
      this.travelService.getOne(id).subscribe((resp)=>{
        this.travelsToAdd = resp;
      })
    })
  } 
    this.categoryService.getCategories().subscribe(
      (resp: Category[]) => {
        this.categories = resp;
   
      });
    this.destinationService.getDestinations().subscribe(
        (resp: Destination[]) => {
          this.destinations = resp;
         
        });      
  }
  byId(category1, category2){
    return category1.id === category2.id;
  }

  save() {

    this.travelService.save(this.travelsToAdd).subscribe(
      (resp) => {});
      this.router.navigate(['/main']);
  
  }
  

}
